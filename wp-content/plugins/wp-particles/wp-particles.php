<?php
/*
Plugin Name: WP Particles
Plugin URI: http://www.farrosoft.com
Description: Simple Wordpress plugin based on lightweight JavaScript library http://vincentgarreau.com/particles.js/ for creating particles.
Version: 1.0
Author: Farrosoft
Author URI: http://www.farrosoft.com
*/

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

define('DOMAIN', 'farrosoft');

require_once(plugin_dir_path(__FILE__) . 'inc/functions.php');
require_once(plugin_dir_path(__FILE__) . 'widget/class-particles-widget.php');
require_once(plugin_dir_path(__FILE__) . 'inc/ajax-functions.php');