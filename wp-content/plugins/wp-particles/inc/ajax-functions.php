<?php
add_action('wp_enqueue_scripts', 'particles_ajax_enqueue_scripts');
function particles_ajax_enqueue_scripts()
{
    wp_enqueue_script('wp-particles-app', plugins_url('/wp-particles/js/particles-app.js'), '', '', true);
    wp_localize_script('wp-particles-app', 'ajax_url', array(
        'url' => admin_url('admin-ajax.php'),
        'json_url' => plugins_url('/wp-particles/js/particles.json')
    ));
}

add_action('wp_ajax_nopriv_particles_data_handler', 'particles_data_handler');
add_action('wp_ajax_particles_data_handler', 'particles_data_handler');

function particles_data_handler()
{
    $data = $_POST['data'];
    $post_id = $data['id'];
    $post_meta = $data['meta'];

    $post_meta = array(
        'id' => $post_id,
        'meta' => get_post_meta($post_id, $post_meta, true)
    );

    print_r(json_encode($post_meta));
    die();
}