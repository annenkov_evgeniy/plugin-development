<?php

new Shortcode_Tinymce();

class Shortcode_Tinymce
{
    public function __construct()
    {
        add_shortcode('particles', array($this,'fs_display_particles'));
        add_action('admin_init', array($this, 'fs_shortcode_button'));
        add_action('admin_footer', array($this, 'fs_get_shortcodes'));
    }

    public function fs_display_particles($options)
    {
        $post = get_post($options['id']);

        extract(shortcode_atts(array(
            'id' => $post->ID,
        ), $options, 'particles-item'));

        $output = '<div data-wrapper-id="' . $post->ID . '"></div>';

        return $output;
    }

    /**
     * Create a shortcode button for tinymce
     *
     * @return [type] [description]
     */
    public function fs_shortcode_button()
    {
        if( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
        {
            add_filter( 'mce_external_plugins', array($this, 'fs_add_buttons' ));
            add_filter( 'mce_buttons', array($this, 'fs_register_buttons' ));
        }
    }

    /**
     * Add new Javascript to the plugin scrippt array
     *
     * @param  Array $plugin_array - Array of scripts
     *
     * @return Array
     */
    public function fs_add_buttons( $plugin_array )
    {
        $plugin_array['fsshortcodes'] = plugins_url('/wp-particles/admin/shortcodes.js');
        return $plugin_array;
    }

    /**
     * Add new button to tinymce
     *
     * @param  Array $buttons - Array of buttons
     *
     * @return Array
     */
    public function fs_register_buttons( $buttons )
    {
        $buttons[] = 'particles';
        return $buttons;
    }

    /**
     * Add shortcode JS to the page
     *
     * @return HTML
     */
    public function fs_get_shortcodes()
    {
        global $post;

        $args = array(
            'post_status' => 'publish',
            'post_type' => 'particles-item',
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'ASC',
        );
        $posts = get_posts($args);

        echo '<script type="text/javascript">
        var shortcodes_button = [];';

        $count = 0;

        foreach($posts as $post)
        {
            setup_postdata($post);
            echo "
            shortcodes_button[{$count}] = new Object({
                name: '{$post->post_title}',
                value: '{$post->ID}'
            }); ";
            $count++;
        }
        wp_reset_postdata();

        echo '</script>';
    }

}