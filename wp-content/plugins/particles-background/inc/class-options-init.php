<?php

new OptionsPage();

class OptionsPage {

    public function __construct() {
        add_action('init', array($this, 'update_options'));
        add_action('admin_menu', array($this, 'register_menu_page'), 5);
        add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_styles'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_frontend_styles'));
    }

    public function register_menu_page() {
        $page_title = __('Particles Background', DOMAIN);
        $menu_title = __('Particles Background', DOMAIN);
        $capability = 'manage_options';
        $menu_slug = 'particles-background';
        $function = array($this, 'render_content');
        $icon_url = 'dashicons-admin-generic';
        $position = 6.3;

        add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position);
    }

    public function enqueue_admin_styles() {
        wp_enqueue_media();
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_style('admin-css', plugins_url('/particles-background/admin/admin.css'));
        wp_enqueue_script('admin-js', plugins_url('/particles-background/admin/admin.js'), array('wp-color-picker'), false, true);
    }

    public function enqueue_frontend_styles() {
        wp_enqueue_style('particles-styles', plugins_url('/particles-background/assets/css/canvas-styles.css'));
    }

    public function update_options() {
        if ($_GET['page'] == 'particles-background') {
            $options = $_POST;
            if (isset($options) && !empty($options)) {
                foreach ($options as $key => $value) {
                    update_option($key, $value);
                }
            }
        }
    }

    public function render_content() {
        ?>
        <div class="fs-content-wrap">
            <header class="fs-header">
                <h1 class="fs-header__title"><?php _e('Particles Background Settings', DOMAIN) ?></h1>
            </header>
            <form method="post" name="particles-background">
                <div class="fs-row">
                    <span class="fs-title"><?php _e('Enable Particles Background', DOMAIN) ?></span>
                    <div class="fs-item-inner">
                        <?php $this->add_logic_control('fs_particles_instance', 'Enable Particles Background', $this->check_logic_instance(get_option('fs_particles_instance'))); ?>
                    </div>
                </div>
                <div class="fs-row">
                    <span class="fs-title"><?php _e('Particles Color', DOMAIN) ?></span>
                    <div class="fs-item-inner">
                        <?php $this->add_color_control('fs_particles_color', 'Particles Color', get_option('fs_particles_color', '#ffffff')); ?>
                    </div>
                </div>
                <div class="fs-row">
                    <span class="fs-title"><?php _e('Particles Opacity', DOMAIN) ?></span>
                    <div class="fs-item-inner">
                        <?php $this->add_range_control('fs_particles_opacity', 'Particles Opacity', get_option('fs_particles_opacity', 0.8), 0, 1, 0.1); ?>
                    </div>
                </div>
                <div class="fs-row">
                    <span class="fs-title"><?php _e('Lines Color', DOMAIN) ?></span>
                    <div class="fs-item-inner">
                        <?php $this->add_color_control('fs_lines_color', 'Lines Color', get_option('fs_lines_color', '#ffffff')); ?>
                    </div>
                </div>
                <div class="fs-row">
                    <span class="fs-title"><?php _e('Lines Opacity', DOMAIN) ?></span>
                    <div class="fs-item-inner">
                        <?php $this->add_range_control('fs_lines_opacity', 'Lines Opacity', get_option('fs_lines_opacity', 0.1), 0, 1, 0.1); ?>
                    </div>
                </div>
                <div>
                    <input type="submit" class="button button-primary button-large"
                           value="<?php _e('Сохранить настройки', DOMAIN) ?>">
                </div>
            </form>
        </div>
        <?php
    }

    public function check_logic_instance($var) {
        return $var == 'true';
    }

    public function add_logic_control($name, $title, $value) {
        ?>
        <label for="<?php echo $name; ?>"><?php _e($title, DOMAIN); ?></label><br/>
        <?php
        $directions = array('true', 'false');
        foreach ($directions as $i => $direction) {
            $val = $value ? 'true' : 'false';
            $attr = $direction == $val ? 'checked' : '';
            $label = $direction == 'true' ? 'ON' : 'OFF';
            ?>
            <input type="radio" id="<?php echo $name; ?><?php echo $i; ?>"
                   name="<?php echo $name; ?>"
                   value="<?php echo $direction; ?>" <?php echo $attr; ?>>
            <label class="button button-default"
                   for="<?php echo $name; ?><?php echo $i; ?>"><?php _e($label, DOMAIN); ?></label>
            <?php
        }
        ?>
        <?php
    }

    public function add_range_control($name, $label, $value, $min, $max, $step) {
        ?>
        <label
            for="<?php echo $name; ?>"><?php _e($label, DOMAIN); ?></label><br/>
        <input class="range" type="range" min="<?php echo $min; ?>" max="<?php echo $max; ?>"
               step="<?php echo $step; ?>"
               id="<?php echo $name; ?>"
               name="<?php echo $name; ?>"
               value="<?php echo $value; ?>"/>
        <span class="value-container"></span>
        <?php
    }
    public function add_number_control($name, $label, $value, $min, $max, $step) {
        ?>
        <label
            for="<?php echo $name; ?>"><?php _e($label, DOMAIN); ?></label><br/>
        <input class="number" type="number" min="<?php echo $min; ?>" max="<?php echo $max; ?>"
               step="<?php echo $step; ?>"
               id="<?php echo $name; ?>"
               name="<?php echo $name; ?>"
               value="<?php echo $value; ?>"/>
        <span class="value-container"></span>
        <?php
    }

    public function add_color_control($name, $label, $value) {
        ?>
        <label for="<?php echo $name; ?>"><?php _e($label, DOMAIN); ?></label><br/>
        <input class="colorpicker" id="<?php echo $name; ?>" name="<?php echo $name; ?>"
               value="<?php echo $value; ?>"/>
        <?php
    }

    public function add_text_control($name, $title, $val='', $placeholder='') {
        ?>
        <label for="<?php echo $name; ?>"><?php _e($title, DOMAIN); ?></label><br/>
        <input type="text" id="<?php echo $name; ?>" name="<?php echo $name; ?>" value="<?php echo $val; ?>" placeholder="<?php echo $placeholder; ?>" />
        <?php
    }

    public function add_textarea_control($name, $title, $value) {
        ?>
        <label for="<?php echo $name; ?>"><?php _e($title, DOMAIN); ?></label><br/>
        <textarea type="text" style="width: 80%;" rows="15" id="<?php echo $name; ?>" name="<?php echo $name; ?>"><?php echo stripslashes($value); ?></textarea>
        <?php
    }

    public function add_radio_control($name, $title, $value, $instances) {
        ?>
        <label
            for="<?php echo $name; ?>"><?php _e($title, DOMAIN); ?></label><br/>
        <?php
        foreach ($instances as $i => $instance) {
            $attr = $instance == $value ? 'checked' : '';
            ?>
            <input type="radio" id="<?php echo $name; ?>_<?php echo $i; ?>"
                   name="<?php echo $name; ?>"
                   value="<?php echo $instance; ?>" <?php echo $attr; ?>>
            <label class="button button-default"
                   for="<?php echo $name; ?>_<?php echo $i; ?>"><?php echo str_replace('_', ' ', $instance ); ?></label>
            <?php
        }
    }

    public function add_checkbox_control($name, $value, $instances) {
        ?>
        <label
            for="<?php echo $name; ?>"><?php _e($name, DOMAIN); ?></label><br/>
        <?php
        foreach ($instances as $i => $instance) {
            $attr = in_array($instance, $value) ? 'checked' : '';
            ?>
            <input type="checkbox" id="<?php echo $name; ?>_<?php echo $i; ?>"
                   name="<?php echo $name; ?>[]"
                   value="<?php echo $instance; ?>" <?php echo $attr; ?>>
            <label class="button button-default"
                   for="<?php echo $name; ?>_<?php echo $i; ?>"><?php echo $instance ?></label>
            <?php
        }
    }
    
    

}