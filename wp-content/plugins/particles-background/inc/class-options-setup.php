<?php

new SetupOptions();

class SetupOptions {

    public function __construct() {
        add_action('init', array($this, 'setOptions'));
        add_action('wp_enqueue_scripts', array($this, 'addCanvasAnimation'));
    }

    public function addCanvasAnimation () {
        if($this->get_option_value('fs_particles_instance') == 'true') {
            wp_enqueue_script('canvas-animation', plugins_url('/particles-background/assets/js/canvas-animation.js'), array('jquery'), '', true);
            wp_localize_script('canvas-animation', 'particlesSettings', array(
                'particles_color' => $this->get_option_value('fs_particles_color') != '' ? $this->get_option_value('fs_particles_color') : '#ffffff',
                'particles_opacity' => $this->get_option_value('fs_particles_opacity') != '' ? $this->get_option_value('fs_particles_opacity') : '0.5',
                'lines_color' => $this->get_option_value('fs_lines_color') != '' ? $this->get_option_value('fs_lines_color'): '#ffffff',
                'lines_opacity' => $this->get_option_value('fs_lines_opacity') != '' ? $this->get_option_value('fs_lines_opacity') : '0.1',
                'is_front_page' => is_front_page()
            ));
        }
    }


    private $definedOptions = array('fs_particles_instance', 'fs_particles_color', 'fs_particles_opacity', 'fs_lines_color', 'fs_lines_opacity');
    private $options;


    public function setOptions() {
        $options = array();

        foreach ($this->definedOptions as $option) {
            $options[$option] = get_option($option);
        }
        $this->options = $options;
    }

    public function getOptions() {
        return $this->options;
    }

    private function get_option_value($name) {
        $options = $this->options;
        if(!empty($options)) {
            foreach ($options as $key => $value) {
                if ($key == $name) {
                    return $value;
                }
            }
        }
        return '';
    }
    
}