<?php
/*
Plugin Name: Particles Background
Plugin URI: http://www.farrosoft.com
Description: Particles Animation Background Plugin
Version: 1.0
Author: Farrosoft
Author URI: http://www.farrosoft.com
*/

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

define('DOMAIN', 'farrosoft');

require_once(__DIR__ . '/inc/functions.php');